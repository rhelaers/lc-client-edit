$("#submit").on("click", function () {
  
  $("#path").html("Thinking...");
  
    
  var start = $("#from").val().trim();
  var destination = $("#to").val().trim();
  


  var planner = new lc.Client({entrypoints: ["http://localhost:3001/"+start+"/"+destination]} );
  //var planner = new lc.Client({entrypoints: ["http://localhost:3001"]} );
  //var planner = new lc.Client({entrypoints: ["http://belgianrail.linkedconnections.org/connections"]} );

  
  var departure=new Date();
  console.log(departure);
  planner.query({
    "departureStop" : start,
    "arrivalStop" : destination,
    "departureTime" : departure
    
//    "departureStop" : "8892007",
//    "arrivalStop" : "8812005",
//    "departureTime" : new Date("2015-10-12T10:00:00.000Z")
  }, function (stream, source) {
      console.log("stream");
      console.log(stream)
    stream.on('result', function (path) {
        console.log("resultaat");
      $("#path").html("");
      if (path) {
        path.forEach(function (connection) {
          $("#path").append(connection.departureTime.toISOString() + " at " + connection.departureStop + " To arrive in " + connection.arrivalStop + " at " +  connection.arrivalTime.toISOString() + "<br/>");
        });
        source.close();
      }
      var duration = ((path[path.length-1].arrivalTime.getTime() - path[0].departureTime.getTime())/60000 );
      $("#path").append("Duration of the journey is: " + duration + " minutes");
    });
    stream.on('data', function (connection) {
      //console.log(connection);
      console.log("data");
    });
    stream.on('error', function (error) {
      console.error(error);
      console.log("error");
    });
    stream.on('end', function () {
      console.log('end of stream');
    });
  });
});

Date.prototype.addHours= function(h){
    this.setHours(this.getHours()+h);
    return this;
};

